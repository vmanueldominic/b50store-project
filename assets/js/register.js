// collect input elements
const uName = document.querySelector("#username");
const email = document.querySelector("#email");
const password1 = document.querySelector("#pword1");
const password2 = document.querySelector("#pword2");
const regBtn = document.querySelector("#submitBtn");

// collect all input fields for attaching an event listener later on
const inputs = document.querySelectorAll("input.form-control");

// create a function that will check for duplicates emails in the database ASYNCHROUNOUSLY
function checkEmail(){
	// javascript's fetch() function accepts a URL (can be absolute or relative) and returns a PROMISE object
	return fetch('../../controllers/check_email.php?email='+email.value)
	// the promise object returned by fetch() may or may not resolve, if it resolves it will pass in its return value as a response to a function passed in to then()
	.then((res)=>{
		return res.text();
	})
	.then((data)=>{
		// error flag, will be toggled to true if any validation fails
		let flag = false;
		// if email is already in the users table
		if (data === 'taken') {	
			email.nextElementSibling.innerHTML = `Email is ${data}`;
			flag = true;
		}else{
			email.nextElementSibling.innerHTML = `Email is ${data}`;
		}
		return flag;
	})
}

// give notification when any input field is left blank
inputs.forEach((input)=>{
	input.addEventListener("input", (e)=>{
		if(e.target.value == ""){
			e.target.nextElementSibling.innerHTML = "This field is required."
		}else{
			e.target.nextElementSibling.innerHTML = "";
		}
		checker();
	})
})

function checker(){
	let errorFlag = false;

	inputs.forEach((input)=>{
		if(input.value == ""){
			errorFlag = true;
		}
	});

	let pwd1 = pword1.value;//initial password
	let pwd2 = pword2.value;//password verification
	// check if password verification matches the given password is NOT empty
	if(pwd1 != "" && pwd1 != pwd2){
		errorFlag = true;
		password2.nextElementSibling.innerHTML = 'verification has to match the initial password.'
	}

	checkEmail()
	.then((data)=>{
		// if BOTH the flag from checkEmail() and the errorFlag in this function are FALSE
		if(!data && !errorFlag){
			regBtn.disabled = false;//enable button
		}else{
			regBtn.disabled = true;
		}
	})
}

// attach our checkEmail() function to an "input" event
// an input event is used on an input HTML and it will trigger whenever the value of that input element is changed