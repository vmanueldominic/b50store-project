<?php
$title = "Registration";

function get_content(){?><!--php ends here to start using html-->
	<div class="row">
		<div class="col-8 offset-2">
			<h1>Registration Form</h1>
			<form method="POST" action="../controllers/register.php">
				<!-- email input -->
				<div class="form-group row">
					<label for="email" class="col-3 text-right">Email</label>
					<input type="email" class="form-control col-9" id="email" name="email">
					<span id="emailError" class="col-9 offset-3"></span>
				</div>
				<!-- username input -->
				<div class="form-group row">
					<label for="username" class="col-3 text-right">Username</label>
					<input type="email" class="form-control col-9" id="username" name="email">
					<span id="usernameError" class="col-9 offset-3"></span>
				</div>
				<!-- password input -->
				<div class="form-group row">
					<label for="pword1" class="col-3 text-right">Password</label>
					<input type="password" class="form-control col-9" id="pword1" name="password">
					<span id="pword1Error" class="col-9 offset-3"></span>
				</div>
				<!-- password verification -->
				<div class="form-group row">
					<label for="pword2" class="col-3 text-right">Verify Password</label>
					<input type="password" class="form-control col-9" id="pword2">
					<span id="pword2Error" class="col-9 offset-3"></span>
				</div>
				<!--form submission button -->
				<button type="submit" id="submitBtn" class="btn btn-primary" disabled>Register</button>
			</form>
		</div>
	</div>
<?php }

require_once "./layouts/app.php";
?>
<script type="text/javascript" src="../assets/js/register.js"></script>
